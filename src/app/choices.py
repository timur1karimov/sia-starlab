from enum import Enum


class GenreChoice(int, Enum):
    NO_GENRE = 1
    FANTASTIC = 2


class BookViewAction(str, Enum):
    ENTITY = "ENTITY"
    VIEW = "VIEW"
    DOWNLOAD = "DOWNLOAD"
