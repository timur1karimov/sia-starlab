from app.routers.author import AuthorCreateListView
from app.routers.book import BookCreateListView, BookView, BookDownloadView, BookViewerView
from app.routers.denied_list import DeniedListView
from app.routers.genre import GenreCreateListView



routes = [
    ('/api/books', BookCreateListView),
    ('/api/books/{book_id}', BookView),
    ('/api/books/{book_id}/download', BookDownloadView),
    ('/api/books/{book_id}/view', BookViewerView),
    ('/api/authors', AuthorCreateListView),
    ('/api/genres', GenreCreateListView),
    ('/api/denied_list', DeniedListView)
]
