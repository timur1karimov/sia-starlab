from typing import List

from aiohttp import web
from aiohttp_pydantic.oas.typing import r200, r201

from app.routers.base import BaseServiceView
from app.schemas.author import AuthorModel
from app.services.authors import AuthorsService



class AuthorCreateListView(BaseServiceView):
    service = AuthorsService

    async def post(self, author: AuthorModel) -> r201[AuthorModel]:
        """
        Add a new author to src
        Tags: Author
        Status Codes:
            201: The author is created
        """
        service = self.service(app=self.request.app)
        new_author = await service.create(author=author)
        return web.json_response(new_author, status=201)

    async def get(self) -> r200[List[AuthorModel]]:
        """
        Get list all authors
        Tags: Author
        """
        service = self.service(app=self.request.app)
        authors = await service.get_list()
        return web.json_response(authors, status=200)

