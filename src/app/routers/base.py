from aiohttp_pydantic import PydanticView
from pydantic import ValidationError



class BaseServiceView(PydanticView):
    service: object

    @staticmethod
    def validate(schema, data: dict) -> tuple:
        errors = []
        validate_json = None
        try:
            validate_json = schema.model_validate(data)
        except ValidationError as e:
            [errors.append({"message": f'{error["loc"][0]} {error["msg"]}'}) for error in e.errors()]
        return errors, validate_json
