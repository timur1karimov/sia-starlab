from typing import List, Optional, Union

from aiohttp import web
from aiohttp_pydantic.oas.typing import r200, r201, r404

from app.choices import BookViewAction
from app.routers.base import BaseServiceView
from app.schemas.book import BookModel, BookCreateModel
from app.services.book import BookService



class BookView(BaseServiceView):
    service = BookService

    async def get(self, book_id: int, /) -> Union[r200[BookCreateModel], r404[BookCreateModel]]:
        """
        Find a book by ID
        Tags: Book
        Status Codes:
            200: Successful operation
            404: Book not found
        """
        service = self.service(app=self.request.app)
        book = await service.get_book(book_id=book_id, action=BookViewAction.ENTITY)
        if not book:
            return web.json_response({"message": "Book not found"}, status=404)
        return web.json_response(book, status=200)


class BookCreateListView(BaseServiceView):
    service = BookService

    async def post(self) -> r201[BookCreateModel]:
        """
        Add a new book to src
        Tags: Book
        Status Codes:
            201: The book is created
        """
        data = await self.request.post()
        attached_file = data.get("file")
        errors, validate_data = self.validate(schema=BookModel, data=dict(data))
        if errors:
            return web.json_response(errors, status=400)
        service = self.service(app=self.request.app)
        new_book = await service.create(book=validate_data)
        if attached_file:
            await service.apply_file(file_content=attached_file, book_id=new_book.get("id"))
        return web.json_response(new_book, status=201)

    async def get(
            self,
            name: Optional[str] = None, author: Optional[str] = None, date_published: Optional[str] = None,
            genre: Optional[str] = None, limit: Optional[str] = None, skip: Optional[str] = None
    ) -> r200[List[BookCreateModel]]:
        """
        Get list all book
        Query params: author, name, date_published, limit, skip
        Tags: Book
        """
        service = self.service(app=self.request.app)
        query_params = dict(
            name=name,
            author=author,
            date_published=date_published,
            genre=genre,
            limit=limit,
            skip=skip
        )
        books = await service.get_list(query_params=query_params)
        return web.json_response(books, status=200)


class BookDownloadView(BaseServiceView):
    service = BookService

    async def get(self, book_id: int, /) -> Union[r200[BookCreateModel], r404[BookCreateModel]]:
        """
        Download book file by ID

        Tags: Book
        Status Codes:
            200: Successful operation
            404: Book not found
        """
        service = self.service(app=self.request.app)
        book = await service.get_book(book_id=book_id, action=BookViewAction.DOWNLOAD)
        if not book:
            return web.json_response({"message": "Book not found"}, status=404)
        return web.Response(
            body=book.get("file"),
            content_type=book.get("file_content_type"),
            headers={'Content-Disposition': 'Attachment'}
        )


class BookViewerView(BaseServiceView):
    service = BookService

    async def get(self, book_id: int, /) -> Union[r200[BookCreateModel], r404[BookCreateModel]]:
        """
        View book file by ID
        Tags: Book
        Status Codes:
            200: Successful operation
            404: Book not found
        """
        service = self.service(app=self.request.app)
        book = await service.get_book(book_id=book_id, action=BookViewAction.VIEW)
        if not book:
            return web.json_response({"message": "Book not found"}, status=404)
        return web.Response(
            body=book.get("file"),
            content_type=book.get("file_content_type")
        )

