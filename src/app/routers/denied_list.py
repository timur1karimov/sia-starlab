from aiohttp import web
from aiohttp_pydantic.oas.typing import r200

from app.routers.base import BaseServiceView
from app.services.denied_list import DeniedListService



class DeniedListView(BaseServiceView):
    service = DeniedListService

    async def post(self) -> r200[dict]:
        """
        Upload and applied denied list
        Tags: Denied list
        Status Codes:
            200: Successful operation
        """
        data = await self.request.post()
        attached_file = data.get("file")
        service = self.service(app=self.request.app)
        if attached_file:
            await service.processing_denied_list(file_content=attached_file)
        return web.json_response({"message": "Denied list applied"}, status=200)
