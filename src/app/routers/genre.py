from typing import List

from aiohttp import web
from aiohttp_pydantic.oas.typing import r200, r201

from app.routers.base import BaseServiceView
from app.schemas.genre import GenreModel
from app.services.genre import GenreService



class GenreCreateListView(BaseServiceView):
    service = GenreService

    async def post(self, genre: GenreModel) -> r201[GenreModel]:
        """
        Add a new genre to src
        Tags: Genre
        Status Codes:
            201: The genre is created
        """
        service = self.service(app=self.request.app)
        new_genre = await service.create(genre=genre)
        return web.json_response(new_genre, status=201)

    async def get(self) -> r200[List[GenreModel]]:
        """
        Get list all genres
        Tags: Genre
        """
        service = self.service(app=self.request.app)
        genres = await service.get_list()
        return web.json_response(genres, status=200)
