from typing import Optional
from pydantic import BaseModel, model_validator


class AuthorModel(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    full_name: Optional[str] = None

    @model_validator(mode="before")
    def _set_fields(cls, values: dict) -> dict:
        first_name = values.get("first_name", "")
        last_name = values.get("last_name", "")
        if all([isinstance(first_name, str), isinstance(last_name, str)]):
            if not values.get("full_name"):
                values["full_name"] = " ".join([first_name, last_name]).strip()
        return values
