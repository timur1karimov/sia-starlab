from datetime import date
from typing import Optional

from aiohttp import web_request
from pydantic import BaseModel

from app.choices import GenreChoice


class BookModel(BaseModel):
    name: str
    author: int
    genre: Optional[GenreChoice] = GenreChoice.NO_GENRE.value
    is_denied: Optional[bool] = False
    date_published: date
    file: Optional[object]

    # class Config:
    #     orm_mode = True
    #     arbitrary_types_allowed = True

class BookCreateModel(BookModel):
    link_view: Optional[str]
    link_download: Optional[str]
