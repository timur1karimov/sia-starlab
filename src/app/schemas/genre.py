from typing import Optional
from pydantic import BaseModel


class GenreModel(BaseModel):
    genre: Optional[str] = None

