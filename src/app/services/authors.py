from typing import List

from sqlalchemy import select

from app.schemas.author import AuthorModel
from app.tables import Author, Base


class AuthorsService:
    plural_for_json = "authors"

    def __init__(self, app):
        self.app = app

    def _list_obj_to_dict(self, list_objs: list) -> dict:
        return {self.plural_for_json: [obj.to_dict() for obj in list_objs if isinstance(obj, Base)]}

    async def create(self, author: AuthorModel) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                new_author = Author(
                    first_name=author.first_name,
                    last_name=author.last_name,
                    full_name=author.full_name
                )
                session.add(new_author)
                await session.commit()
                return new_author.to_dict()

    async def get_list(self) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                query = await session.execute(select(Author).order_by(Author.last_name))
                await session.commit()
                all_authors_objs = query.scalars().all()
                return self._list_obj_to_dict(list_objs=all_authors_objs)
