import io
from sqlalchemy import select
from aiohttp.web_request import FileField

from app.choices import BookViewAction
from app.schemas.book import BookModel
from app.tables import Base, Book, Author, Genre



class BookService:
    plural_for_json = "books"

    def __init__(self, app):
        self.app = app

    async def apply_file(self, file_content: FileField, book_id: int) -> None:
        content_type = file_content.content_type
        file_content = file_content.file.read()
        buf = io.BytesIO(file_content)
        async with self.app.db_connector() as session:
            async with session.begin():
                query = await session.execute(select(Book).filter(Book.id == book_id).limit(1))
                book = query.scalars().one()
                book.file = buf.getvalue()
                book.file_content_type = content_type
                await session.commit()

    async def apply_denied_list(self, file_content: FileField, book_id: int) -> None:
        content_type = file_content.content_type
        file_content = file_content.file.read()
        buf = io.BytesIO(file_content)
        async with self.app.db_connector() as session:
            async with session.begin():
                query = await session.execute(select(Book).filter(Book.id == book_id).limit(1))
                book = query.scalars().one()
                book.file = buf.getvalue()
                book.file_content_type = content_type
                await session.commit()

    def _list_obj_to_dict(self, list_objs: list) -> dict:
        exceptions_list = ["author_id", "genre_id", "file", "file_content_type", "is_denied"]
        container = []
        for obj in list_objs:
            d = dict()
            for item in obj:
                if isinstance(item, Base):
                    item_dict = item.to_dict_nested(exceptions_list=exceptions_list)

                    d = {**d, **item_dict}
            container.append(d)
        return {self.plural_for_json: container}

    async def create(self, book: BookModel) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                new_book = Book(
                    name=book.name,
                    author_id=book.author,
                    genre_id=book.genre,
                    is_denied=book.is_denied,
                    date_published=book.date_published,

                )
                session.add(new_book)
                await session.commit()
                res = new_book.to_dict()
                return res

    async def get_list(self, query_params: dict) -> dict:
        def _applying_filters(base_query, skip: int = 0, limit: int = 10):
            if query_params.get("name"):
                base_query = base_query.filter(Book.name.match(query_params.get("name")))
            if query_params.get("date_published"):
                base_query = base_query.filter(Book.date_published == query_params.get("date_published"))
            if query_params.get("author"):
                base_query = base_query.filter(Author.full_name.match(query_params.get("author")))
            if query_params.get("genre"):
                base_query = base_query.filter(Genre.genre.match(query_params.get("genre")))
            if query_params.get("limit"):
                limit_val = query_params.get("limit")
                if limit_val.isdigit():
                    limit = int(limit_val)
                base_query = base_query.limit(limit)
            if query_params.get("skip"):
                skip_val = query_params.get("skip")
                if skip_val.isdigit():
                    skip = int(skip_val)
                base_query = base_query.offset(skip)
            return base_query

        async with self.app.db_connector() as session:
            async with session.begin():
                query = (
                    select(Book, Author, Genre)
                    .join(Author, Book.author_id == Author.id)
                    .join(Genre, Book.genre_id == Genre.id)
                    .order_by(Book.name)
                )
                result = await session.execute(_applying_filters(base_query=query))
                await session.commit()
                return self._list_obj_to_dict(list_objs=result.all())

    async def get_book(self, book_id: int, action: BookViewAction) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                query = select(Book).filter(Book.id == int(book_id))
                result = await session.execute(query)
                await session.commit()
                obj: Book = result.scalars().first()
                if obj:
                    return obj.get_book_structure(action=action)
