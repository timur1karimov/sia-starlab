import io
from typing import Optional, List

from sqlalchemy import update, select
from aiohttp.web_request import FileField
from app.tables import Book, Author
import openpyxl



class ProcessingManager:
    target: List[dict] = None

    def __init__(self, obj):
        self._obj = obj
        self._wb_obj = self._load_workbook()

    def _load_workbook(self):
        return openpyxl.load_workbook(filename=self._obj)

    @staticmethod
    def _extract_values(tab, column_name: str) -> Optional[list]:
        extract_values = list()
        for column_cell in tab.iter_cols(1, tab.max_column):
            if column_cell[0].value == column_name:
                for data in column_cell[1:]:
                    extract_values.append(data.value)
                break
        return extract_values

    def get_entity_and_values(self) -> dict:
        result = dict()
        for target in self.target:
            for tab, column in target.items():
                select_tab = self._wb_obj[tab]
                if select_tab:
                    result[tab] = self._extract_values(tab=select_tab, column_name=column)
        return result


class DeniedListManager(ProcessingManager):
    target = [
        {"name": "name"},
        {"author": "name"}
    ]


class DeniedListService:

    def __init__(self, app):
        self.app = app
        self.processing_manager = DeniedListManager

    async def _apply_denied_list(self, entity_and_values: dict) -> None:
        for entity, values in entity_and_values.items():
            values = [str(v) for v in values]
            async with self.app.db_connector() as session:
                async with session.begin():
                    if entity == "name":
                        await session.execute(
                            update(Book)
                            .filter(Book.name.in_(values))
                            .values({"is_denied": True})
                            .execution_options(synchronize_session='fetch')
                        )
                    if entity == "author":
                        author_q = (
                            select(Author.id)
                            .filter(Author.full_name.in_(values))
                        )
                        await session.execute(
                            update(Book)
                            .filter(Book.author_id.in_(author_q))
                            .values({"is_denied": True})
                            .execution_options(synchronize_session='fetch')
                        )
                    await session.commit()

    async def processing_denied_list(
            self, file_content: FileField
    ) -> None:
        file_content = file_content.file.read()
        buf = io.BytesIO(file_content)
        entity_and_values: dict = self.processing_manager(obj=buf).get_entity_and_values()
        await self._apply_denied_list(entity_and_values=entity_and_values)
