
from sqlalchemy import select

from app.schemas.genre import GenreModel
from app.tables import Base, Genre


class GenreService:
    plural_for_json = "genres"

    def __init__(self, app):
        self.app = app

    def _list_obj_to_dict(self, list_objs: list) -> dict:
        return {self.plural_for_json: [obj.to_dict() for obj in list_objs if isinstance(obj, Base)]}

    async def create(self, genre: GenreModel) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                new_genre = Genre(
                    genre=genre.genre,
                )
                session.add(new_genre)
                await session.commit()
                return new_genre.to_dict()

    async def get_list(self) -> dict:
        async with self.app.db_connector() as session:
            async with session.begin():
                query = await session.execute(select(Genre).order_by(Genre.genre))
                await session.commit()
                all_objs = query.scalars().all()
                return self._list_obj_to_dict(list_objs=all_objs)
