import os
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DATABASE_URL: str = os.getenv(
        'DATABASE_URL', "postgresql+asyncpg://db_user:db_pass@database_project:5432/src"
    )
    DATABASE_TEST_URL: str = os.getenv(
        'DATABASE_URL', "postgresql+asyncpg://db_user:db_pass@localhost:5460/library_test"
    )


settings = Settings()
