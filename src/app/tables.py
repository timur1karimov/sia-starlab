from datetime import date
from sqlalchemy import MetaData, ForeignKey
from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped, relationship
from typing import Optional, List

from app.choices import BookViewAction

meta = MetaData()


class Base(AsyncAttrs, DeclarativeBase):
    def to_dict(self, exceptions_list: list = None):
        if not exceptions_list:
            exceptions_list = []
        form_dict = {k: v for k, v in self.__dict__.items() if not k.startswith("_") and k not in exceptions_list}
        for k, v in form_dict.items():
            if isinstance(v, date):
                form_dict[k] = str(v)
        return form_dict

    def to_dict_nested(self, exceptions_list: list = None):
        return {self.__class__.__name__.lower(): self.to_dict(exceptions_list=exceptions_list)}


class Genre(Base):
    __tablename__ = "genre"

    id: Mapped[int] = mapped_column(primary_key=True)
    genre: Mapped[Optional[str]]
    book: Mapped[List["Book"]] = relationship(back_populates="genre")

    def __repr__(self) -> str:
        return f"Genre(id={self.id!r}, genre={self.genre!r})"


class Author(Base):
    __tablename__ = "author"

    id: Mapped[int] = mapped_column(primary_key=True)
    first_name: Mapped[Optional[str]]
    last_name: Mapped[Optional[str]]
    full_name: Mapped[Optional[str]]
    book: Mapped[List["Book"]] = relationship(back_populates="author")

    def __repr__(self) -> str:
        return f"Author(id={self.id!r}, first_name={self.first_name!r}, " \
               f"last_name={self.last_name!r}, full_name={self.full_name!r})"


class Book(Base):
    __tablename__ = "book"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[Optional[str]]
    author_id: Mapped[int] = mapped_column(ForeignKey("author.id"))
    author: Mapped["Author"] = relationship(back_populates="book")
    genre_id: Mapped[Optional[int]] = mapped_column(ForeignKey("genre.id"))
    genre: Mapped[Optional["Genre"]] = relationship(back_populates="book")
    file: Mapped[Optional[bytes]]
    file_content_type: Mapped[Optional[str]]
    is_denied: Mapped[bool]
    date_published: Mapped[date]

    def __get_book_entity(self) -> dict:
        book_dict = self.to_dict(exceptions_list=["file", "file_content_type", "is_denied"])
        if self.file:
            book_dict["link_view"] = f"/api/books/{self.id}/view"
            if not self.is_denied:
                book_dict["link_download"] = f"/api/books/{self.id}/download"
        return book_dict

    def __get_book_for_view(self) -> Optional[dict]:
        book_dict = self.to_dict()
        if self.file:
            return book_dict

    def __get_book_for_download(self) -> Optional[dict]:
        book_dict = self.to_dict()
        if self.file:
            if not self.is_denied:
                return book_dict

    def get_book_structure(self, action: BookViewAction) -> Optional[dict]:
        mapping_by_action = {
            BookViewAction.ENTITY: self.__get_book_entity,
            BookViewAction.VIEW: self.__get_book_for_view,
            BookViewAction.DOWNLOAD: self.__get_book_for_download,
        }
        obj = mapping_by_action.get(action)
        return obj()

    def __repr__(self) -> str:
        return f"Book(id={self.id!r}, name={self.name!r}, author={self.author!r}, genre={self.genre!r})"
