from typing import AsyncIterator

from aiohttp import web
from aiohttp_pydantic import oas
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

from app.routers import routes
from app.settings import settings
from app.tables import Base



async def init_db(app: web.Application) -> AsyncIterator[None]:
    engine = create_async_engine(
        app["db_config"],
    )
    app.db_connector = async_sessionmaker(engine, expire_on_commit=False)
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    # await engine.close()



async def init_app(test: bool = False) -> web.Application:
    app = web.Application()
    app["db_config"] = settings.DATABASE_URL if not test else settings.DATABASE_TEST_URL
    app.cleanup_ctx.append(init_db)
    [app.router.add_view(rout[0], rout[1]) for rout in routes if isinstance(rout, tuple)]
    oas.setup(app, url_prefix='/api/docs', title_spec="e-src", version_spec="0.1.1")
    return app


if __name__ == "__main__":
    web.run_app(init_app(), port=8000)
