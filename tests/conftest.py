import pytest
from main import init_app



@pytest.fixture
async def cli(aiohttp_client):
    return await aiohttp_client(await init_app(test=True), server_kwargs={'port': 8027})
