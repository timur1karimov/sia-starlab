import json

from pytest_aiohttp.plugin import aiohttp_client



async def test_add_author(cli: aiohttp_client) -> None:
    url = "/api/authors"
    data = {
        "first_name": "Scott",
        "last_name": "Fitzgerald"
    }
    resp = await cli.post(
        url,
        data=json.dumps(data)
    )
    r = await resp.json()
    assert resp.status == 201
    assert r.get("first_name") == "Scott"
    assert r.get("last_name") == "Fitzgerald"


async def test_authors_list(cli: aiohttp_client) -> None:
    url = "/api/authors"
    resp = await cli.get(url)
    r = await resp.json()
    assert resp.status == 200
    assert isinstance(r.get("authors"), list)
