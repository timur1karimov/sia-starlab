import random

import aiohttp
import pytest
from pytest_aiohttp.plugin import aiohttp_client



class TestBookApi:
    @pytest.fixture
    async def test_add_book(self, cli: aiohttp_client):
        form = aiohttp.FormData(quote_fields=False)
        user_data = dict(
            name="The Great Gatsby",
            author=str(1),
            genre=str(1),
            date_published="1925-10-25",
        )
        for key, value in user_data.items():
            form.add_field(key, value)

        form.add_field(
            name="file",
            value=str(object()),
            filename="book.txt",
            content_type="text/plain",
        )
        resp = await cli.post('/api/books', data=form)
        r = await resp.json()
        assert resp.status == 201
        assert r.get("name") == "The Great Gatsby"
        return r.get("id")

    async def test_check_book(self, cli: aiohttp_client, test_add_book):
        resp = await cli.get(f"/api/books/{test_add_book}")
        r = await resp.json()
        assert resp.status == 200
        assert r.get("id") == test_add_book
        assert r.get("link_view") == f"/api/books/{test_add_book}/view"
        assert r.get("link_download") == f"/api/books/{test_add_book}/download"
        assert r.get("author_id") == 1

    async def test_view_book(self, cli: aiohttp_client, test_add_book):
        resp = await cli.get(f"/api/books/{test_add_book}")
        r = await resp.json()
        assert resp.status == 200
        assert r.get("id") == test_add_book
        assert r.get("link_view") == f"/api/books/{test_add_book}/view"
        link_for_view = r.get("link_view")
        resp_ = await cli.get(link_for_view)
        assert resp_.status == 200
        assert resp_.content_type == "text/plain"

    async def test_filters_book(self, cli: aiohttp_client, test_add_book):
        resp = await cli.get(f"/api/books?author=Scott Fitzgerald")
        r = await resp.json()
        assert resp.status == 200
        assert isinstance(r.get("books"), list)
        assert len(r.get("books")) != 0


    async def test_check_book_with_random_id(self, cli: aiohttp_client):
        resp = await cli.get(f"/api/books/{random.randint(1099, 99912)}")
        r = await resp.json()
        assert resp.status == 404
        assert r == {'message': 'Book not found'}
