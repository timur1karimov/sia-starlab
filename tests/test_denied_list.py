import aiohttp
from pytest_aiohttp.plugin import aiohttp_client



class TestDeniedListApi:

    async def test_denied_list(self, cli: aiohttp_client):
        form = aiohttp.FormData(quote_fields=False)
        form.add_field(
            name="file",
            value=open("tests/publisher_decline_list.xlsx", "rb"),
            filename="decline_list.xlsx",
            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'",
        )
        resp = await cli.post('/api/denied_list', data=form)
        r = await resp.json()
        assert resp.status == 200
        assert r == {'message': 'Denied list applied'}
