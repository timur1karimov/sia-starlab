import json
from pytest_aiohttp.plugin import aiohttp_client



async def test_add_genre(cli: aiohttp_client) -> None:
    url = "/api/genres"
    data = {
        "genre": "Mystic",
    }
    resp = await cli.post(
        url,
        data=json.dumps(data)
    )
    r = await resp.json()
    assert resp.status == 201
    assert r.get("genre") == "Mystic"


async def test_genres_list(cli: aiohttp_client) -> None:
    url = "/api/genres"
    resp = await cli.get(url)
    r = await resp.json()
    assert resp.status == 200
    assert isinstance(r.get("genres"), list)
